$(document).ready(function() {
	var isMobile = false;

	if( window.innerWidth < 800 ) {
		isMobile = true;
	}

	// Parallax stuff
	(function scroller() {

		if( isMobile )
			return;

		var _this = this;
		var events = [];

		var docHeight = $(document).height();
		var windowHeight = $(window).height();
		var docTop = $(document).scrollTop();
		var docBottom = docTop + windowHeight;
		

		// Main handlers
		var handlers = {
			offsetTop: function(item, percentage, marginTop) {
				var amount = $(item).data('percentage') || 100;

				if(percentage > 0) {
					$(item).css(
						'margin-top',
						'calc(' 
							+ marginTop 
							+ ' + '
							+ (percentage * amount) + 
						'%)'
					);
						// .css('margin-top', initialMarginTop)
						// .css('margin-top', (percentage * amount) + '%');
				}
			}
		};

		// Event handlers
		function handleScroll() {
			docTop = $(document).scrollTop();
			docBottom = docTop + windowHeight;

			events.forEach(function(event) {
				if(docBottom >= event.top && docTop <= event.bottom) {
					var start = event.top;
					var end = event.bottom + windowHeight;

					event.handler(
						event.item,
						(1 - ((end / docBottom) - 1)),
						event.marginTop
					);
				}
			});
		}

		$(window).on('resize', function() {
			windowHeight = $(window).height();
			docBottom = docTop + windowHeight;
		});

		$(document).on('scroll', handleScroll);

		(function init() {
			$(".hasScrollEvent").each(function(i, item) {
				var handler = handlers[$(item).data('type')];
				var offsetTop = $(item).offset().top;

				if(handler) {
					events.push({
						handler: handler,
						top: offsetTop,
						bottom: offsetTop + $(item).outerHeight(),
						item: item,
						marginTop: $(item).css('margin-top')
					});
				}
			});

			handleScroll();
		})();
	})();


	// Mobile menu handling
	(function handleMobile() {
		var isOpen = false;

		$(".mobileMenu__button").click(function() {
			if( isOpen ) {
				// $(".mobileMenu .menu").css("height", "0px");
				$(".mobileMenu .menu").animate({height: 0}, 0.4);
				isOpen = false;
			} else {
				// $(".mobileMenu .menu").css("height", "auto");
				$(".mobileMenu .menu").animate({height: 280}, 0.4);
				isOpen = true;
			}
		});
	})()
});