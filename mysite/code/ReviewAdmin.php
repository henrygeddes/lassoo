<?php 

use SilverStripe\Admin\ModelAdmin;

class ReviewAdmin extends ModelAdmin 
{

    private static $managed_models = [
        'Review'
    ];

    private static $url_segment = 'reviews';

    private static $menu_title = 'Reviews';
}