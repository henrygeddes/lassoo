<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\Form;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

use SilverStripe\Control\Email\Email;
use SilverStripe\Forms\RequiredFields;

class ContactController extends PageController
{
    private static $allowed_actions = ['Form', 'submit'];

    protected function init()
    {
        parent::init();
    }

    public function Form() 
    { 
    	$validator = new RequiredFields('Name', 'Email', 'Message');
        $fields = new FieldList( 
            new TextField('Name'), 
            new EmailField('Email'),
            new TextField('Content'),
            new TextareaField('Message')
        ); 
        $actions = new FieldList( 
            new FormAction('submit', 'Submit') 
        ); 
        return new Form($this, 'Form', $fields, $actions, $validator); 
    }

    public function submit($data, $form) 
    { 
        $email = new Email(); 

        if( $data['Content'] )
        	return [ 'Content' => 'Thank you for your feedback', 'Form' => ''];

        $email->setTo('neil@lassoo.biz'); 
        $email->setFrom($data['Email']); 
        $email->setSubject("Contact Message from {$data["Name"]}"); 

        $messageBody = " 
            <p><strong>Name:</strong> {$data['Name']}</p> 
            <p><strong>Message:</strong> {$data['Message']}</p> 
        "; 
        $email->setBody($messageBody); 
        $email->send();

        return [
            'Content' => 'Thank you for your feedback',
            'Form' => ''
        ];
    }
}
