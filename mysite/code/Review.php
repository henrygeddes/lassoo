<?php 

use SilverStripe\ORM\DataObject;
use SilverStripe\Assets\Image;

class Review extends DataObject 
{

    private static $db = [
        'Name' => 'Varchar',
        'Content' => 'Varchar',
    ];

    private static $has_one = [
        'Image' => Image::class,
    ];

    public function getCMSFields() 
    {
        $fields = parent::getCMSFields();
        $fields->dataFieldByName('IsActive')->setTitle('Is active?');

        return $fields;
    }
}
