<?php

use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\TextField;
use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\FormAction;
use SilverStripe\Forms\Form;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

use SilverStripe\Assets\Image;

class Contact extends Page
{
    private static $db = [
    	
    ];

    private static $defaults = [
    	
    ];

    private static $has_one = [
    	
    ];

    public function getCMSFields() 
	{
		$fields = parent::getCMSFields();

		$fields->removeFieldFromTab("Root.Main", "Content_1");
		$fields->removeFieldFromTab("Root.Main", "Content_2");

		return $fields;
	}
}
