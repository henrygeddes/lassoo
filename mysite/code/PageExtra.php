<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Assets\Image;
use Silverstripe\Forms\TextField;
use Silverstripe\Forms\TextareaField;
use Silverstripe\Forms\CheckboxField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class PageExtra extends SiteTree
{
	private static $db = [
    	'HeroText' 			=> 'Text',
        'HeroContent' => 'HTMLText',
        'PopoutContent' => 'HTMLText',
    ];

    private static $defaults = [
    ];

    private static $has_one = [
    	'HeroImage' 			=> Image::class,
        'ContentImage1'             => Image::class,
        'ContentImage2'             => Image::class,
        'ContentImage3'             => Image::class,
    ];

    public function getCMSFields() 
	{
		$fields = parent::getCMSFields();

        $main = $fields->fieldByName('Root')->fieldByName('Main');
        $main->setTitle('Meta');


        // / / / / / / / / / / / / / / / / / / / / / / / / / 
        // Setup our tabs
        $fields->addFieldsToTab("Root.HeroBlock", [
            TextareaField::create('HeroText', 'Hero Text'),
            HTMLEditorField::create('HeroContent', 'Content'),
            UploadField::create('HeroImage', 'Hero Image'),
        ]);
        
        $fields->addFieldsToTab("Root.Images", [
            UploadField::create('ContentImage1', 'Image1'),
            UploadField::create('ContentImage2', 'Image2'),
            UploadField::create('ContentImage3', 'Image3'),
        ]);

        $fields->addFieldsToTab("Root.PopoutContent", [
            HTMLEditorField::create('PopoutContent', 'Content'),
        ]);

		return $fields;
	}
}
