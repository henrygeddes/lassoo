<?php

use Silverstripe\Forms\TextField;
use Silverstripe\Forms\TextareaField;
use Silverstripe\Forms\CheckboxField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

use SilverStripe\Assets\Image;

class Home extends Page
{
    private static $db = [
    	'HeroText' 			=> 'HTMLText',

    	'ImageBlockHeader' 	=> 'HTMLText',
    	'ImageBlockContent_1' 	=> 'HTMLText',
    	'ImageBlockContent_2' => 'HTMLText',
    	'ImageBlockContent_3' 	=> 'HTMLText',
    	'ImageBlockContent_4' => 'HTMLText',

    	'ImageBlockLink1' => 'HTMLText',
    	'ImageBlockLink2' => 'HTMLText',
    	'ImageBlockLink3' => 'HTMLText',
    	'ImageBlockLink4' => 'HTMLText',

    	'ReviewContent' 	=> 'HTMLText',

    	'BlockContent_1' 	=> 'HTMLText',
    	'BlockContent_2' 	=> 'HTMLText',
    ];

    private static $defaults = [
    	'EnableColumns_7' => TRUE,
    ];

    private static $has_one = [
    	'HeroImage' 			=> Image::class,
    	'ReviewImage' 			=> Image::class,
    	'BlockContentImage_1' 	=> Image::class,
    	'BlockContentImage_2' 	=> Image::class,
    	'ImageBlock1' 			=> Image::class,
    	'ImageBlock2' 			=> Image::class,
    	'ImageBlock3' 			=> Image::class,
    	'ImageBlock4' 			=> Image::class,
    ];

    public function getCMSFields() 
	{
		$fields = parent::getCMSFields();


		// / / / / / / / / / / / / / / / / / / / / / / / / / 
		// Remove main content editor
		$fields->removeByName("Root.Content", "Content");
		$fields->removeFieldFromTab("Root.Main", "Content");
		$fields->removeFieldFromTab("Root.Main", "Content_1");
		$fields->removeFieldFromTab("Root.Main", "Content_2");
		$fields->removeFieldFromTab("Root.Main", "Image");
		// Rename Main tab to Meta
		$main = $fields->fieldByName('Root')->fieldByName('Main');
		$main->setTitle('Meta');


		// / / / / / / / / / / / / / / / / / / / / / / / / / 
		// Setup our tabs
		$fields->addFieldsToTab("Root.HeroBlock", [
			TextareaField::create('HeroText', 'Hero Text'),
			UploadField::create('HeroImage', 'Hero Image'),
			HTMLEditorField::create('ImageBlockHeader', 'Header'),
		]);
		
		$fields->addFieldsToTab("Root.ImageBlock1", [
			HTMLEditorField::create('ImageBlockContent_1', 'Content'),
			UploadField::create('ImageBlock1', 'Image'),
			TextareaField::create('ImageBlockLink1', 'More Link'),
		]);

		$fields->addFieldsToTab("Root.ImageBlock2", [
			HTMLEditorField::create('ImageBlockContent_2', 'Column'),
			UploadField::create('ImageBlock2', 'Image'),
			TextareaField::create('ImageBlockLink2', 'More Link'),
		]);

		$fields->addFieldsToTab("Root.ImageBlock3", [
			HTMLEditorField::create('ImageBlockContent_3', 'Column'),
			UploadField::create('ImageBlock3', 'Image'),
			TextareaField::create('ImageBlockLink3', 'More Link'),
		]);

		$fields->addFieldsToTab("Root.ImageBlock4", [
			HTMLEditorField::create('ImageBlockContent_4', 'Column'),
			UploadField::create('ImageBlock4', 'Image'),
			TextareaField::create('ImageBlockLink4', 'More Link'),
		]);

		$fields->addFieldsToTab("Root.ReviewContent", [
			HTMLEditorField::create('ReviewContent', 'Column'),
			UploadField::create('ReviewImage', 'Image'),
		]);
		

		$fields->addFieldsToTab("Root.BlockContent1", [
			HTMLEditorField::create('BlockContent_1', 'Column'),
			UploadField::create('BlockContentImage_1', 'Image'),
		]);

		$fields->addFieldsToTab("Root.BlockContent2", [
			HTMLEditorField::create('BlockContent_2', 'Column'),
			UploadField::create('BlockContentImage_2', 'Image'),
		]);

		return $fields;
	}
}
