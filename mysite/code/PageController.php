<?php

use SilverStripe\CMS\Controllers\ContentController;

class PageController extends ContentController
{
    /**
     * An array of actions that can be accessed via a request. Each array element should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * [
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
     *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
     * ];
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = [];

    protected function init()
    {
        parent::init();
        // You can include any CSS or JS required by your project here.
        // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/
    }

    public function titleBr() {
        // $chunks = explode(" ", strtolower($this->title));

        // if( count($chunks) === 2 )
        //     return implode("\n", $chunks);

        // if( count($chunks) === 3 ) {
        //     if( strlen($chunks[2]) > 6 )
        //         return "$chunks[0] $chunks[1]\n$chunks[2]";

        //     return "$chunks[0]\n$chunks[1] $chunks[2]";
        // }

        return strtolower($this->title);
    }
}
