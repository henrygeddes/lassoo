<?php

use SilverStripe\CMS\Model\SiteTree;
use SilverStripe\Assets\Image;
use Silverstripe\Forms\TextField;
use Silverstripe\Forms\TextareaField;
use Silverstripe\Forms\CheckboxField;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;

class Page extends SiteTree
{
	private static $db = [
    	'HeroText' 			=> 'HTMLText',
    	 
    	// 'BlockHeaderColor_1' 		=> 'Text',
    	// 'BlockBackgroundColor_1' 	=> 'Text',
    
    	// 'DiagramHeader' 	=> 'Text',
    	// 'BlockHeader_1' 	=> 'Text',
    	// 'BlockHeader_2' 	=> 'Text',
    	// 'BlockHeader_3' 	=> 'Text',
    	// 'BlockHeader_4' 	=> 'Text',
    	

    	// 'BlockColumnFirst_1' 	=> 'HTMLText',
    	// 'BlockColumnSecond_1' 	=> 'HTMLText',
    	// 'BlockColumnFirst_2' 	=> 'HTMLText',
    	// 'BlockColumnSecond_2' 	=> 'HTMLText',
    	// 'BlockColumnFirst_3' 	=> 'HTMLText',
    	// 'BlockColumnSecond_3' 	=> 'HTMLText',
    	// 'BlockColumnFirst_4' 	=> 'HTMLText',
    	// 'BlockColumnSecond_4' 	=> 'HTMLText',	


    	// "EnableBlock_1"		=> "Boolean",
    	// "EnableBlock_2"		=> "Boolean",
    	// "EnableBlock_3"		=> "Boolean",
    	// "EnableBlock_4"		=> "Boolean",
    	// "EnableHowItWorks"	=> "Boolean",


    	// "EnableColumns_1" 	=> "Boolean",
    	// "EnableColumns_2" 	=> "Boolean",
    	// "EnableColumns_3" 	=> "Boolean",
    	// "EnableColumns_4" 	=> "Boolean",
    ];

    private static $defaults = [
    	// 'EnableBlock_1' => FALSE,
    	// 'EnableBlock_2' => FALSE,
    	// 'EnableBlock_3' => FALSE,
    	// 'EnableBlock_4' => FALSE,

    	// 'EnableColumns_1' => FALSE,
    	// 'EnableColumns_2' => FALSE,
    	// 'EnableColumns_3' => FALSE,
    	// 'EnableColumns_4' => FALSE,
    ];

    private static $has_one = [
    	'CoreImage' 			=> Image::class,
  //   	'DiagramImageDesktop' 	=> Image::class,
		// 'DiagramImageMobile' 	=> Image::class,
    ];

    public function getCMSFields() 
	{
		$fields = parent::getCMSFields();

        $fields->addFieldsToTab("Root.CoreImage", [
            HTMLEditorField::create('HeroText', 'Hero Text'),
            UploadField::create('CoreImage', 'Hero Image'),
        ]);

		return $fields;
	}
}
